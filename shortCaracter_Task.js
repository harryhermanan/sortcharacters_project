function shortCaracters(word) {
    return word.split('').sort().join('');
}

console.log(shortCaracters('hello')); //'ehllo'
console.log(shortCaracters('truncate')); // 'acenrttu'
console.log(shortCaracters('developer')); // 'deeeloprv'
